#!/bin/bash
MQTTSERVERIP=localhost   #IP or hostname of the MQTT-Server
MQTTSERVERPORT=1883          #TCP Port of the MQTT-Server
MQTTBASETOPIC=sensor1  #Base topic of the Subjects to subscribe
HTMLFILEDESTINATION=/var/www/html/index.html    # Set to the absolut path of index.html 
HTMLTEMPLATE=`dirname $0`/../etc/website.template.html  #Place the template html here


SCRIPTNAME=`basename $0`
PIDFILE=/tmp/$SCRIPTNAME.pid



# Print Usage information and exit with exitcode
usage(){
    echo "usage $SCRIPTNAME <start|stop|status>" >&2
    exit $1
}

# outputs 0 if the script is not running 
# outputs PID of the process if the script

getpid(){

    # Check if a PIDFILE exists
    if [ -f $PIDFILE ] ; then
        PID=`cat $PIDFILE`
        # Check if process is running
        if [ -x /proc/$PID ] ; then
            # Check if command is really the same script
            if [ "$(cat /proc/$PID/comm)" = "$(cat /proc/$$/comm)" ] ; then
                echo $PID
		return
            fi
        fi
    fi
    echo 0    # The script can not run on PID 0 !!!
    return
}

log(){
    echo "$(date '+%Y%m%d%H%M%S'):$*" >&2
}

run(){
    # This routine will change all the template file for all topics recieved
    # when the topic end is recieved it will place the resulting file to the docroot

    # Copy the template file to a temporary file
    log "Starting..."
    cp $HTMLTEMPLATE $HTMLTEMPLATE.tmp

    # Subscribe all topics below MQTTBASETOPIC/*
    mosquitto_sub -h $MQTTSERVERIP -p $MQTTSERVERPORT -t "$MQTTBASETOPIC/#" -F "%t %p" |while read topic payload ; do
        log "Got new message on topic $topic with payload $payload"
        case $topic in
            # If the end topic is recieved put the temporary file to the destination
            # and restart with a new template
            $MQTTBASETOPIC/end)
                log "Install new file $HTMLFILEDESTINATION"
                mv $HTMLTEMPLATE.tmp $HTMLFILEDESTINATION
                cp $HTMLTEMPLATE $HTMLTEMPLATE.tmp 
                ;;
            # any other topic use the last part of the string as pattern to replace by payload
            $MQTTBASETOPIC/temp|$MQTTBASETOPIC/press|$MQTTBASETOPIC/humid)
                stringname=${topic#$MQTTBASETOPIC/}
                log "Replacing _${stringname}_ with $payload"
                sed -i "s/_${stringname}_/$payload/g" $HTMLTEMPLATE.tmp 
                ;;
            # take payload of weatherdata topic as comma seperated values
            $MQTTBASETOPIC/weatherdata)
                log "Replacing _temp_,_press_,_humid_ with $payload"
                temperature=`echo $payload|cut -d , -f 1`
                pressure=`echo $payload|cut -d , -f 2`
                humidity=`echo $payload|cut -d , -f 3`
                sed -i "s/_temp_/$temperature/g" $HTMLTEMPLATE.tmp 
                sed -i "s/_press_/$pressure/g" $HTMLTEMPLATE.tmp
                sed -i "s/_humid_/$humidity/g" $HTMLTEMPLATE.tmp
                log "Install new file $HTMLFILEDESTINATION"
                mv $HTMLTEMPLATE.tmp $HTMLFILEDESTINATION
                cp $HTMLTEMPLATE $HTMLTEMPLATE.tmp 
                ;;
            *)
                log "Unexpected topic recieved: $topic"
                ;;
        esac
    done
}



# Parse the arguments given on command line
case $1 in 
   start) 
        # Always check if the script is already running
        PID=$(getpid)
        if [ $PID -ne 0 ] ; then
            echo "$SCRIPTNAME is already running with PID $PID"
            exit 1
        fi
        # If not, just start the background process run
        run & 
        PID=$!
        echo $PID > /tmp/$SCRIPTNAME.pid
        ;;
   stop) 
        # Stop the process if it is running
        PID=$(getpid)
        if [ $PID -ne 0 ] ; then
            pkill -P $PID
            # rm $PIDFILE
        fi
        
   ;;
   status) 
        # Run getpid to see if process is running
        PID=$(getpid)
        if [ $PID -ne 0 ] ; then
            log "$SCRIPTNAME is running with PID $PID"
            exit 0
        else
            log "$SCRIPTNAME is not running"
            exit 1
        fi     
   ;;
   *) usage 1 ;;
esac
