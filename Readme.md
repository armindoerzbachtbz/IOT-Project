# Cloud-Init Repo für den IOT-Projekt-Auftrag
Dieses Repo ist eine mögliche Lösung zum [IOT-Projekt-Auftrag](https://gitlab.com/armindoerzbachtbz/cloud-native-bootcamp/-/tree/main/projects/IOT-Projekt).

Im Verzeichnis cloud-init findet ihr das [init.yml](cloud-init/init.yml) file welches ihr 1 zu 1 in die User-Data Section einer neuen EC2-VM kopieren könnt. **Wichtig:** erste Zeile mitkopieren.

Ausser der Security-Group für die EC2-VM müsst ihr nichts zusätzlich machen.


